/**
* Created by s25854 on 19/10/2016.
*/
// var express = require("express");
//
// const NODE_PORT = 3000;
//
// var app = express();
//
// app.use(express.static(__dirname + "/../client/"));
//
// app.listen(NODE_PORT, function () {
//     console.log("Server running at http://localhost:" + NODE_PORT);
// });

const path = require("path");
const express = require("express");
const app = express();
const docroot = path.join(__dirname, "..");

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.use(express.static(path.join(docroot, "client")));

app.use("/bower_components",
    express.static(path.join(docroot, "bower_components")));

app.listen(app.get("port"), function () {
    console.log("Application started on port %d", app.get("port"));
});
