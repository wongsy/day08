/**
 * Created by s25854 on 19/10/2016.
 */
(function(){
   var MyApp = angular.module("MyApp",[]);
   var MyCtrl = function() {
       var myCtrl = this;
       // myCtrl.message = "";
       //
       // myCtrl.toUpperCase = function(){
       //     myCtrl.message= myCtrl.message.toUpperCase();
       // }

       myCtrl.item = ""; //create models
       myCtrl.items = [];

       myCtrl.addToCart = function () {
           console.info("Adding to cart");
           myCtrl.items.push(myCtrl.item);
           myCtrl.item = "";
       }
       myCtrl.deleteItem = function(index){
           myCtrl.items.splice(index, 1);
       }

       //Alternative:
       //  angular.module("MyApp",[])
       //      .controller("MyCtrl",[]);
   }

    MyApp.controller("MyCtrl", [ MyCtrl ]);

})();
